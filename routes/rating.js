const express = require('express');
const router = express.Router();
const middlewares = require('../utils/middlewares');

router.post('/:id', middlewares.getRecipe, middlewares.checkRatingValid, middlewares.checkRatingExists, async (req, res) => {
    res.recipe.ratings.push({ userEmail: req.userEmail, rating: res.rating });
    try {
        res.json(await res.recipe.save());
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

module.exports = router;
