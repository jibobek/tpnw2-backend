require('dotenv').config();
const express = require('express');
const router = express.Router();
const User = require('../model/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/', async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    if (user === null) {
        res.status(400).send("User not found");
        return;
    }
    try {
        if (await bcrypt.compare(req.body.password, user.password)) {
            res.json({ accessToken: generateAccessToken(user) });
        } else {
            res.status(403).send("Bad password");
        }
    } catch (e) {
        res.status(500).send();
    }
});

function generateAccessToken(user) {
    return jwt.sign(user.email, process.env.ACCESS_TOKEN_SECRET);
}

module.exports = router;
