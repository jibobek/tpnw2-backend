const express = require('express');
const router = express.Router();
const Recipe = require('../model/recipe');
const middlewares = require('../utils/middlewares');

const ITEMS_PER_PAGE = 20;

router.get("/", middlewares.checkPage, async (req, res) => {
    res.send(await Recipe.find({ userEmail: req.userEmail }).limit(ITEMS_PER_PAGE).skip(req.offset));
});

module.exports = router;
