const express = require('express');
const router = express.Router();
const Comment = require('../model/comment');
const middlewares = require('../utils/middlewares');

router.get("/:id", middlewares.getRecipe, async (req, res) => res.send(await Comment.find({ recipeId: res.recipe._id }).sort({ date: "desc" }).limit(100)));

module.exports = router;
