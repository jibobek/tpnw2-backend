const express = require('express')
const router = express.Router()
const Recipe = require('../model/recipe')
const middlewares = require('../utils/middlewares');

router.get("/:id", middlewares.getRecipe, async (req, res) => res.json(res.recipe));
module.exports = router;
