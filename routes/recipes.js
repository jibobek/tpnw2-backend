const express = require('express');
const router = express.Router();
const Recipe = require('../model/recipe');
const middlewares = require('../utils/middlewares');

const ITEMS_PER_PAGE = 20;

router.get("/", middlewares.checkPage, async (req, res) => {
    let search = req.query.search || "";
    res.send(await Recipe.find({ isPrivate: false, name: { $regex: new RegExp("^" + search.toLowerCase(), "i") } }).limit(ITEMS_PER_PAGE).skip(req.offset))
});

router.get("/list", middlewares.checkPage, async (req, res) => {
    let search = req.query.search || "";
    const recipes = await Recipe.find({ isPrivate: false, name: { $regex: new RegExp("^" + search.toLowerCase(), "i") } }).select({
        name: 1,
        images: 1,
        ratings: 1,
        isPrivate: 1
    }).limit(ITEMS_PER_PAGE).skip(req.offset);
    res.send(recipes);
});

module.exports = router;
