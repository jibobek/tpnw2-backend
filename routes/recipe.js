const express = require('express')
const router = express.Router()
const Recipe = require('../model/recipe')
const middlewares = require('../utils/middlewares');

router.get("/:id", middlewares.getRecipe, async (req, res) => res.json(res.recipe));

router.post('/', async (req, res) => {
    const recipe = new Recipe({
        userEmail: req.userEmail,
        name: req.body.name,
        description: req.body.description,
        steps: req.body.steps,
        images: req.body.images,
        ingredients: req.body.ingredients,
        isPrivate: req.body.isPrivate,
    });
    try {
        res.status(201).json(await recipe.save());
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.put('/:id', middlewares.getRecipe, middlewares.checkRecipeOwnership, async (req, res) => {
    if (req.body.name != null) {
        res.recipe.name = req.body.name;
    }
    if (req.body.description != null) {
        res.recipe.description = req.body.description;
    }
    if (req.body.steps != null) {
        res.recipe.steps = req.body.steps;
    }
    if (req.body.ingredients != null) {
        res.recipe.ingredients = req.body.ingredients;
    }
    if (req.body.images != null) {
        res.recipe.images = req.body.images;
    }
    if (req.body.isPrivate != null) {
        res.recipe.isPrivate = req.body.isPrivate;
    }
    try {
        res.json(await res.recipe.save());
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.delete('/:id', middlewares.getRecipe, middlewares.checkRecipeOwnership, async (req, res) => {
    try {
        await res.recipe.remove()
        res.json({ message: 'Deleted recipe' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
})

module.exports = router;
