const express = require('express');
const router = express.Router();
const User = require('../model/user');
const bcrypt = require('bcrypt');

router.post('/', async (req, res) => {
    let hashedPassword;
    if (req.body.password === undefined) {
        res.status(400);
        return
    }
    try {
        hashedPassword = await bcrypt.hash(req.body.password, 10);
    } catch {
        return res.status(500);
    }

    const user = new User({
        email: req.body.email,
        password: hashedPassword,
    })
    try {
        await user.save();
        res.status(201).json({ message: "User registered" });
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
    res.status(200);
});

module.exports = router;
