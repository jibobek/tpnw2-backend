const express = require('express');
const router = express.Router();
const Comment = require('../model/comment');
const middlewares = require('../utils/middlewares');

router.post('/:id', middlewares.getRecipe, middlewares.getUser, async (req, res) => {
    const comment = new Comment({
        userId: res.userId,
        recipeId: res.recipe._id,
        text: req.body.text
    });
    try {
        res.status(201).json(await comment.save());
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.get("/:id", middlewares.getRecipe, async (req, res) => res.send(await Comment.find({ recipeId: res.recipe._id }).sort({ date: "desc" }).limit(100)));

module.exports = router;
