require('dotenv').config();
const cors = require('cors');
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const middlewares = require('./utils/middlewares');
app.use(cors());
app.use(express.json({limit: '50mb'}));

mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to Database'));
app.use(express.json());

const routesAuthenticated = {
    "/recipe": require('./routes/recipe'),
    "/rating": require('./routes/rating'),
    "/comment": require('./routes/comment'),
    "/myrecipes": require('./routes/myrecipes'),
};
const routesUnauthenticated = {
    "/publicrecipe": require('./routes/recipe'),
    "/publiccomments": require('./routes/publiccomments'),
    "/register": require('./routes/register'),
    "/login": require('./routes/login'),
    "/recipes": require('./routes/recipes'),
};

Object.keys(routesAuthenticated).forEach(key => app.use(key, middlewares.authenticateToken, routesAuthenticated[key]));
Object.keys(routesUnauthenticated).forEach(key => app.use(key, routesUnauthenticated[key]));
app.use('*', (req, res) => res.sendStatus(404).send("Url not found"));

const PORT = process.env.PORT || 3000;
app.listen(PORT, err => {
    if(err) throw err;
    console.log("%c Server running", "color: green");
});
