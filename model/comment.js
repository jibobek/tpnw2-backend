const mongoose = require('mongoose')

const { Schema } = mongoose;

const commentSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    recipeId: {
        type: Schema.Types.ObjectId,
        ref: "Recipe",
        required: true
    },
    date: {
        type: Date,
        required: true,
        default: Date.now
    },
    text: {
        type: String,
        required: [true, "Comment must have text"],
        trim: true,
        minLength: 3,
        maxLength: 1024,
    }
});

module.exports = mongoose.model('Comment', commentSchema);
