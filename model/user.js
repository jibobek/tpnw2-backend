const mongoose = require('mongoose')
const validator = require('validator');

const { Schema } = mongoose;

const userSchema = new Schema({
    email: {
        type: String,
        index: true,
        required: [true, "User must have email"],
        trim: true,
        unique: [true, "User is already registered"],
        minLength: 3,
        maxLength: 32,
        validate: (v) => validator.isEmail(v)
    },
    password: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('User', userSchema);
