const mongoose = require('mongoose')
const validator = require('validator');
const Comment = require('../model/comment')

const { Schema } = mongoose;

const recipeSchema = new Schema({
    userEmail: {
        type: String,
        required: [true, "Unexpected error, recipe doesnt have userEmail"],
        validate: (v) => validator.isEmail(v)
    },
    name: {
        type: String,
        index: true,
        required: [true, "Recipe must have name"],
        trim: true,
        unique: [true, "Recipe name must be unique"],
        minLength: 3,
        maxLength: 128,
    },
    description: {
        type: String,
        required: false,
        trim: true,
        unique: [true, "Recipe description must be unique"],
        minLength: 3,
        maxLength: 1024,
    },
    steps: {
        type: [String],
    },
    ingredients: {
        type: [{ quantity: Number, unit: String, name: String }],
        required: true,
        validate: (v) => v.length > 0
    },
    ratings: {
        type: [{ userEmail: String, rating: Number }]
    },
    images: {
        type: [String],
        validate: {
            validator: (v) => validateImages(v)
        }
    },
    isPrivate: {
        type: Boolean,
        default: true,
        required: true
    }
}, {
    toObject: { virtuals: true }, toJSON: { virtuals: true }
});

recipeSchema.virtual("rating").get(function () {//arrow funkce ma prazdny obj v this
    if (this.ratings) {
        let ratings = [];
        this.ratings.forEach(r => ratings.push(r.rating));
        const sum = ratings.reduce((a, b) => a + b, 0);
        const avg = (sum / ratings.length) || 0;
        return +avg.toFixed(2);
    }
    return 0;
});

function validateImages(array) {
    let isValid = true;
    array.forEach(e => {
        if (validator.isBase64(e) === false) {
            //isValid = false;
        }
    });
    return isValid;
}

module.exports = mongoose.model('Recipe', recipeSchema);
