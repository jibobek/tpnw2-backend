const Recipe = require('../model/recipe');
const User = require('../model/user');
const jwt = require('jsonwebtoken');

const getRecipe = async (req, res, next) => {
    let recipe;
    try {
        recipe = await Recipe.findById(req.params.id);
        if (recipe == null) {
            return res.status(404).json({ message: 'Cannot find recipe' });
        }
    } catch (err) {
        return res.status(500).json({ message: err.message });
    }
    res.recipe = recipe;
    next();
}

const getUser = async (req, res, next) => {
    const user = await User.findOne({ email: req.userEmail });
    if (user === null) {
        res.status(400).send("User not found");
        return;
    }
    res.userId = user._id;
    next();
}

const checkPage = async (req, res, next) => {
    const page = +(req.query.page ?? 1);
    if (isNaN(page)) {
        res.sendStatus(400);
        return;
    }
    req.offset = (page - 1) * 20;
    next();
}

const checkRecipeOwnership = async (req, res, next) => {
    if (res.recipe.userEmail !== req.userEmail) {
        return res.status(403).json({ message: 'Recipe has different owner' });
    }
    next();
}

const checkRatingValid = (req, res, next) => {
    if (req.body.rating == null) {
        return res.status(400).json({ message: 'Rating not provided' });
    }
    const rating = +(req.body.rating ?? 1) | 0;
    if (isNaN(rating) || rating > 5 || rating < 1) {
        return res.status(400).json({ message: 'Rating is not valid' });
    }
    res.rating = rating;
    next();
}

const checkRatingExists = (req, res, next) => {
    if (res.recipe.ratings.some(r => r.userEmail === req.userEmail)) {
        return res.status(400).json({ message: 'Already rated' });
    }
    next();
}

const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) {
        return res.sendStatus(401);
    }
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
        if (err) {
            return res.sendStatus(403).send("Hash is not valid");
        }
        req.userEmail = user;
        next();
    });
}

module.exports = {
    getRecipe: getRecipe,
    getUser: getUser,
    checkPage: checkPage,
    checkRecipeOwnership: checkRecipeOwnership,
    checkRatingValid: checkRatingValid,
    checkRatingExists: checkRatingExists,
    authenticateToken: authenticateToken,
};
